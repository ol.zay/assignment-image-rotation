#include "rotate_image.h"

#include <malloc.h>

static void construct_img(struct image* img, struct image const* source) {
    img->width = source->height;
    img->height = source->width;
    img->pixels = malloc(sizeof(struct pixel) * source->width * source->height);
}

struct image rotate(struct image const* source ) {
    struct image output = {0};
    construct_img(&output, source);
    for (size_t i = 0; i < source->width; i++) {
        for (size_t j = 0; j < source->height; j++) {
            output.pixels[i * output.width + j] =  (source->pixels[(source->height - 1 - j) * source->width + i]);
        }
    }
    return output;
}

