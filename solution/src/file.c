#include "file_open_close.h"

bool open_file(FILE **file, const char *name, const char *mode) {
    *file = fopen(name, mode);
    return *file != NULL;
}

bool close_file(FILE **file) {
    if (file == NULL) {
        return 0;
    }
    return !fclose(*file);
}

