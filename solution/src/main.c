#include "rotate_image.h"
#include "bmp_read_write.h"
#include "file_open_close.h"

#include <assert.h>
#include <malloc.h>

int main(int args, char **argv) {

    if (args != 3) {
        printf("Not enough arguments");
		return 1;
    }
	char *file_input = argv[1];
	char *file_output = argv[2];
	FILE* in = NULL;
	FILE* out = NULL;
	static const char cant_open_io_files[] = "Error! Can't open input or/and output file";
	static const char cant_write_out_file[] = "Error! Can't write to the output file";
	static const char cant_close_io_files[] = "Error! Can't close input or/and output file";
	static const char pointer_err[] = "Error! Can't read the input file: file or/and picture structure pointers are empty";
	static const char header_err[] = "Error! Can't read the input file: invalid header";
	static const char pixels_err[] = "Error! Can't read the input file";

	
	if (!open_file(&in, file_input, "rb") || !open_file(&out, file_output, "wb")) {
		printf("%s", cant_open_io_files);
		return 1;
	}
	struct image image = {0};
	enum read_status from_bmp_status = from_bmp(in, &image);
	if (from_bmp_status == READ_INVALID_POINTER) {
		printf("%s", pointer_err);
		if (!close_file(&in) || !close_file(&out)) {
			printf("%s", cant_close_io_files);
		}
		free(image.pixels);
		return 1;
	}
	if (from_bmp_status == READ_INVALID_HEADER) {
		printf("%s", header_err);
		if (!close_file(&in) || !close_file(&out)) {
			printf("%s", cant_close_io_files);
		}
		free(image.pixels);
		return 1;
	}
	if (from_bmp_status == READ_PIXELS_ERROR) {
		printf("%s", pixels_err);
		if (!close_file(&in) || !close_file(&out)) {
			printf("%s", cant_close_io_files);
		}
		free(image.pixels);
		return 1;
	}
	struct image rotated_image = rotate(&image);
	enum write_status to_bmp_status = to_bmp(out, &rotated_image);
	if (to_bmp_status == WRITE_ERROR) {
		printf("%s", cant_write_out_file);
		if (!close_file(&in) || !close_file(&out)) {
			printf("%s", cant_close_io_files);
		}
		free(image.pixels);
		free(rotated_image.pixels);
		return 1;
	}
	if (!close_file(&in) || !close_file(&out)) {
		printf("%s", cant_close_io_files);
		free(image.pixels);
		free(rotated_image.pixels);
		return 1;
	}
	free(image.pixels);
	free(rotated_image.pixels);
	return 0;
	
}

