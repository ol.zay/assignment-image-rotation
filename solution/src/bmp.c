#include "struct_bmp_header.h"
#include "bmp_read_write.h"

#include <assert.h>
#include <malloc.h>
#include <stdio.h>

#define BMP_TYPE 0x4D42
#define BMP_PLANES 1
#define BMP_HEADER_SIZE 40
#define BMP_BIT_COUNT 24

static enum read_status read_header(FILE* in, struct bmp_header* header) {
    size_t fread_r = fread(header, sizeof(struct bmp_header), 1, in);
	if (fread_r < 1) {
		return READ_INVALID_HEADER;
	}
    return READ_OK;
}

static enum read_status read_pixels(struct image* img, FILE* in, uint8_t padding) {
	const size_t width = img->width;
    const size_t height = img->height;
    struct pixel* pixels = malloc(width * height * sizeof (struct pixel));
	if (pixels == NULL) {
		free(pixels);
		return READ_PIXELS_ERROR;
	}
	for (size_t i = 0; i < height; i++) {
		size_t fread_res = fread(pixels + i * width, sizeof(struct pixel), width, in);
		if (fread_res < width) {
			free(pixels);
			return READ_PIXELS_ERROR;
		}
		int fseek_res = fseek(in, padding, SEEK_CUR);
		if (fseek_res != 0) {
			free(pixels);
			return READ_PIXELS_ERROR;
		}
	}
	img->pixels = pixels;
	return READ_OK;
}



static uint8_t get_padding(uint32_t width) {
	return width % 4;
}

static void init_img(struct image* img, struct bmp_header header) {
	img->width = header.biWidth;
	img->height = header.biHeight;
}


enum read_status from_bmp(FILE* in, struct image* img) {
	if (in == NULL || img == NULL) {
		return READ_INVALID_POINTER;
	}
	struct bmp_header header = {0};
	if (read_header(in, &header) == READ_INVALID_HEADER) {
		return READ_INVALID_HEADER;
	}
	init_img(img, header);
	return read_pixels(img, in, get_padding(header.biWidth));
}



static struct bmp_header create_header(const struct image* img) {
	uint32_t img_size = (sizeof(struct pixel) * img->width + get_padding(img->width)) * img->height;
	struct bmp_header header = {
		.bfType = BMP_TYPE,
		.bfileSize = sizeof(struct bmp_header) + img_size,
		.bfReserved = 0,
		.bOffBits = sizeof(struct bmp_header),
		.biSize = BMP_HEADER_SIZE,
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = BMP_PLANES,
		.biBitCount = BMP_BIT_COUNT,
		.biCompression = 0,
		.biSizeImage = img_size,
		.biXPelsPerMeter = 0,
		.biYPelsPerMeter = 0,
		.biClrUsed = 0,
		.biClrImportant = 0,

	};
	return header;
}



static enum write_status write_image(struct image const *img, FILE *out/*, uint8_t padding*/) {
	const size_t width = img->width;
    const size_t height = img->height;
	const uint8_t padding = width % 4;
	uint64_t zero = 0;
	uint64_t cnt = 0;
	for (size_t i = 0; i < height; ++i) {
		size_t fwrite_res1 = fwrite(img->pixels + i * width, sizeof (struct pixel), width, out);
		if (fwrite_res1 < width) {
			return WRITE_ERROR;
		}
		cnt += fwrite_res1;
		size_t fwrite_res2 = fwrite(&zero, 1, padding, out);
		if (fwrite_res2 < padding) {
			return WRITE_ERROR;
		}
	}
	if (cnt == height * width) {
		return WRITE_OK;
	}
	return WRITE_ERROR;
}




enum write_status to_bmp(FILE* out, struct image* img) {
	if (out == NULL || img == NULL) {
		return WRITE_ERROR;
	}
	struct bmp_header header = create_header(img);
	if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
		return WRITE_ERROR;
	}
	/*const uint8_t padding = get_padding(header.biWidth); */
	return write_image(img, out /*, padding */);	
}

